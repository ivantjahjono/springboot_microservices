//package com.in28minutes.rest.webservices.restfulwebservices;
//
//import org.springframework.context.annotation.Configuration;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
//
////this class is needed for Spring version >2.0.0
//@Configuration
//public class SecurityConfig extends WebSecurityConfigurerAdapter {
//
//	@Override
//	protected void configure(HttpSecurity http) throws Exception {
//		// TODO Auto-generated method stub
//		http.csrf().disable()
//		.authorizeRequests()
//		.anyRequest().authenticated()
//		.and()
//		.httpBasic();
//	}
//	
//}
