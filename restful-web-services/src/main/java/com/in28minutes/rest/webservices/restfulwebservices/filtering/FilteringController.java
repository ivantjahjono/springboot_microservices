package com.in28minutes.rest.webservices.restfulwebservices.filtering;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;

@RestController
public class FilteringController {
	
	//field1, field2
	@GetMapping("/filtering")
	public MappingJacksonValue retrieveSomeBean() {
		SomeBean someBean = new SomeBean("value1", "value2", "value3");
		
		HashSet<String> filteringSet = new HashSet<String>(Arrays.asList("field1", "field2"));
		
		MappingJacksonValue mapping = applyFiltering(filteringSet, someBean);
		
		return mapping;
	}
	
	//field2, field3
	@GetMapping("/filtering-list")
	public MappingJacksonValue retrieveListOfSomeBean() {
		List<SomeBean> list = Arrays.asList(new SomeBean("value1", "value2", "value3"), 
				new SomeBean("value12", "value22", "value32"));
		
		HashSet<String> filteringSet = new HashSet<String>(Arrays.asList("field2","field3")); 
		
		MappingJacksonValue mapping = applyFiltering(filteringSet, list);
		
		return mapping;
	}
	
	private MappingJacksonValue applyFiltering(Set<String> filterList, Object mappingObj) {
		SimpleBeanPropertyFilter filter = SimpleBeanPropertyFilter.
				filterOutAllExcept(filterList);
		
		FilterProvider filters = 
				new SimpleFilterProvider()
				.addFilter("SomeBeanDynamicFilter", filter);
		
		MappingJacksonValue mapping = new MappingJacksonValue(mappingObj);
		mapping.setFilters(filters);
		
		return mapping;
	}
}
