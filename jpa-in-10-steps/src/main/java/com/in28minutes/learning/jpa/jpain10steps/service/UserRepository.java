package com.in28minutes.learning.jpa.jpain10steps.service;

import org.springframework.data.jpa.repository.JpaRepository;

import com.in28minutes.learning.jpa.jpain10steps.entity.User;

public interface UserRepository extends JpaRepository<User, Long>{
	/*
	 * we're using Spring Data JPA instead of the 
	 * manual EntityManager. With Spring Data JPA, you just
	 * need to define interface, and Spring Data JPA helps to connect
	 * with EntityManager
	 */
}
