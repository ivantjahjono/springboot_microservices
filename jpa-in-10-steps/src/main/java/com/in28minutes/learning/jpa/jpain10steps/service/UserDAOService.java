package com.in28minutes.learning.jpa.jpain10steps.service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.in28minutes.learning.jpa.jpain10steps.entity.User;

//repository annotion save user and retrieve user from database


/*
 * Transactional annotation is because when we need to insert 
 * data to database, we need to perform open and close transaction
 * which is a pain to write the code repeatedly for all methods.
 * So this annotation used for the class (can be placed on a method)
 * indicates that all methods of this class are transactional
 */
@Repository
@Transactional
public class UserDAOService {
	/*
	 * this annotation is to ensure that object instance
	 * that is being persist by the EntityManager is 
	 * being tracked/autowired when its attributes change.
	 * So in this case, the instance user is being tracked.
	 */
	@PersistenceContext
	private EntityManager entityManager;
	
	public long insert(User user) {
		entityManager.persist(user);
		return user.getId();
	}
}
