package com.in28minutes.microservices.netflixzuulapigatewayserver;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;

@Component
public class ZuulLoggingFilter extends ZuulFilter{

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Override
	public boolean shouldFilter() {
		//if you want to execute this filter
		return true;
	}

	@Override
	public Object run() throws ZuulException {
		HttpServletRequest request = 
				RequestContext.getCurrentContext().getRequest();
		logger.info("request -> {} request uri -> {}", 
				request, request.getRequestURI());
		
		return null;
	}

	@Override
	public String filterType() {
		/*
		 * to indicate when should the filtering is done
		 * before request is executed, after, or only on error, or all
		 * in this example, we use before request is executed
		 */
		return "pre";
	}

	@Override
	public int filterOrder() {
		//order of this filter
		return 1;
	}

}
