package com.in28minutes.microservices.currencyconversionservice;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class CurrencyConversionController {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private CurrencyExchangeServiceProxy proxy;
	
	@GetMapping("/currency-converter/from/{from}/to/{to}/quantity/{quantity}")
	public CurrencyConversionBean convertCurrency(
			@PathVariable String from,
			@PathVariable String to,
			@PathVariable BigDecimal quantity) {
		
		/*
		 * Feign
		 * Makes it easy to invoke other microservices
		 * Integration with client side load balancing called Ribbon
		 * 
		 */
		
		Map<String, String> uriVariables = new HashMap<>();
		uriVariables.put("from", from);
		uriVariables.put("to", to);
		
		ResponseEntity<CurrencyConversionBean> responseEntity =
		new RestTemplate().getForEntity(
				"http://localhost:8000/currency-exchange/from/{from}/to/{to}", 
				CurrencyConversionBean.class,
				uriVariables);
		
		CurrencyConversionBean response = responseEntity.getBody();
		
		return new CurrencyConversionBean(
				response.getId(), from, to,response.getConversionMultiple(),
				quantity,quantity.multiply(response.getConversionMultiple()),
				response.getPort());
	}
	//to route the request to go through API gateway, use the following request URI
	//http://localhost:8765/currency-conversion-service/currency-converter-feign/from/EUR/to/INR/quantity/1000
	@GetMapping("/currency-converter-feign/from/{from}/to/{to}/quantity/{quantity}")
	public CurrencyConversionBean convertCurrencyFeign(
			@PathVariable String from,
			@PathVariable String to,
			@PathVariable BigDecimal quantity) {
		
		/*
		 * Feign
		 * Makes it easy to invoke other microservices
		 * Integration with client side load balancing called Ribbon
		 * 
		 * Ribbon allows 1 microservice to talk to all instances of the called
		 * microservices
		 */
		
		
		CurrencyConversionBean response = proxy.retrievedExchangeValue(from, to);
		
		logger.info("{}", response);
		
		return new CurrencyConversionBean(
				response.getId(), from, to,response.getConversionMultiple(),
				quantity,quantity.multiply(response.getConversionMultiple()),
				response.getPort());
	}
}
