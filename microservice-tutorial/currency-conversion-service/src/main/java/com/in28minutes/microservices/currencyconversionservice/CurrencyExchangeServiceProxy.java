package com.in28minutes.microservices.currencyconversionservice;

import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

//the name is from the application.properties application name
//feign library is used to make RESTful calls much easier by eliminating boilerplate code
//@FeignClient(name="currency-exchange-service", url="localhost:8000")
//if you use ribbon, you don't have to specify the url="localhost:port" anymore
//ribbon is a client side load-balancing library that is able to distribute 
//request load to different instances of service running on different ports
//@FeignClient(name="currency-exchange-service")
@FeignClient(name="netflix-zuul-api-gateway-server") //to route your request to zuul api gateway server
@RibbonClient(name="currency-exchange-service")
public interface CurrencyExchangeServiceProxy {
	/*
	 * The method name doesn't have to be the same as the one in CurrencyExchangeController
	 * However, the parameter formating has to be the same 
	 */
	//@GetMapping("/currency-exchange/from/{from}/to/{to}")
	
	//this is the URI to route your request to Zuul
	@GetMapping("currency-exchange-service/currency-exchange/from/{from}/to/{to}")
	public CurrencyConversionBean retrievedExchangeValue(@PathVariable String from, 
			@PathVariable String to);
}
